AddCSLuaFile()

DEFINE_BASECLASS("base_anim")
ENT.PrintName = "MRE Entity"
ENT.Information = "A MRE entity"
ENT.Category = "ES Military Food Mod"
ENT.Spawnable = true
ENT.AdminOnly = false
local Sound = Sound("physics/cardboard/cardboard_box_break1.wav")
function ENT:SpawnFunction( ply, tr, ClassName )

	if ( !tr.Hit ) then return end

	local size = math.random( 16, 48 )

	local ent = ents.Create( ClassName )
	ent:SetPos( tr.HitPos + tr.HitNormal * size )

	ent:Spawn()
	ent:Activate()

	return ent

end

function ENT:Use( activator, caller )
    self:EmitSound( Sound )
	self:Remove()
    
	if ( activator:IsPlayer() ) then

		local health = activator:Health()
		activator:SetHealth( health + 35 )
		

	end

end

function ENT:Initialize()
	-- Sets what model to use
	self:SetModel("models/electrosssnake/mre-electrosssnake.mdl")
    
	-- Physics stuff
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )

	-- Init physics only on server, so it doesn't mess up physgun beam
	if ( SERVER ) then self:PhysicsInit( SOLID_VPHYSICS ) end

	-- Make prop to fall on spawn
	local phys = self:GetPhysicsObject()
	if ( IsValid( phys ) ) then phys:Wake() end
end



